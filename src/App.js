import SumComponent from "./components/SumComponent";
import React, { Component } from "react";

export class App extends Component {
  state = {
    numbers: [],
    numbersSelected: false
  };

  addNumber = number => {
    const newNumber = {
      value: number,
      selected: false
    };

    this.setState({ numbers: [...this.state.numbers, newNumber] });
  };

  deleteSelected = () => {
    this.setState({
      numbers: [
        ...this.state.numbers.filter(number => number.selected === false)
      ],
      numbersSelected: false
    });
  };

  toggleSelected = index => {
    let result = this.state.numbers;
    result[index].selected = !result[index].selected;

    this.setState({ numbers: result });
  };

  toggleNumbersSelected = () => {
    for (var i = 0; i < this.state.numbers.length; i++) {
      if (this.state.numbers[i].selected === true) {
        this.setState({ numbersSelected: true });
        break;
      } else {
        this.setState({ numbersSelected: false });
      }
    }
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div className="container">
            <SumComponent
              numbers={this.state.numbers}
              addNumber={this.addNumber}
              toggleSelected={this.toggleSelected}
              numbersSelected={this.state.numbersSelected}
              toggleNumbersSelected={this.toggleNumbersSelected}
              deleteSelected={this.deleteSelected}
            />
          </div>
        </header>
      </div>
    );
  }
}

export default App;
