import React, { Component } from "react";

export class Numbers extends Component {
  toggleNumberState = index => {
    this.props.toggleSelected(index);
    this.props.toggleNumbersSelected();
  };

  switchNumberStyle = number => {
    if (number.selected === true) {
      return {
        backgroundColor: "#d9f1fd",
        border: "1px",
        borderStyle: "solid",
        borderColor: "#aacedb",
        padding: "0.25em"
      };
    }
  };

  showDeleteBtn = () => {
    if (this.props.numbersSelected) {
      return (
        <button
          type="button"
          className="btn btn-danger"
          onClick={this.props.deleteSelected}
        >
          DELETE SELECTED
        </button>
      );
    }
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm">
            <h3>List of values</h3>
            <div className="myContainer">
              <ul id="number-list">
                {this.props.numbers.map((number, index) => (
                  <li
                    key={index}
                    className="text-primary "
                    style={this.switchNumberStyle(number)}
                    onClick={() => this.toggleNumberState(index)}
                  >
                    {number.value}
                  </li>
                ))}
              </ul>
            </div>
          </div>
          <div className="col-sm">{this.showDeleteBtn()}</div>
        </div>
      </div>
    );
  }
}

export default Numbers;
