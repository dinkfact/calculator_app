import React, { Component } from "react";

export class AddNumber extends Component {
  state = {
    numberToAdd: ""
  };

  onChange = e => {
    this.setState({ numberToAdd: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();
    this.props.addNumber(this.state.numberToAdd);
    this.setState({ numberToAdd: "" });
  };

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <div className="container">
          <div className="row">
            <div className="col-sm">
              <input
                type="number"
                className="form-control"
                id="inputNumber"
                value={this.state.numberToAdd}
                onChange={this.onChange}
              />
            </div>
            <div className="col-sm">
              <button type="submit" className="btn btn-primary">
                ADD
              </button>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

export default AddNumber;
