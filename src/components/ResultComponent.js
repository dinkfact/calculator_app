import React, { Component } from "react";

export class ResultComponent extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-6">
            <h3>Total value</h3>
            <div className="myContainer">
              <span className="text-primary">{this.props.sum}</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ResultComponent;
