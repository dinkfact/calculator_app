import React, { Component } from "react";
import AddNumber from "./AddNumber";
import Numbers from "./Numbers";
import ResultComponent from "./ResultComponent";

export class SumComponent extends Component {
  calcSum = numbers => {
    let result = 0;

    numbers.map(number => {
      result += parseInt(number.value);
    });

    return result;
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-6">
            <h3>MY SUM</h3>
          </div>
        </div>
        <br />
        <AddNumber addNumber={this.props.addNumber} />
        <br />
        <Numbers
          numbers={this.props.numbers}
          toggleSelected={this.props.toggleSelected}
          numbersSelected={this.props.numbersSelected}
          toggleNumbersSelected={this.props.toggleNumbersSelected}
          deleteSelected={this.props.deleteSelected}
        />
        <br />
        <ResultComponent sum={this.calcSum(this.props.numbers)} />
      </div>
    );
  }
}

export default SumComponent;
